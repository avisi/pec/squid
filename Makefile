
build:
	docker build -t registry.gitlab.com/avisi/pec/squid:latest .

run:
	docker run -it --rm -p 3128:3128 --name squid registry.gitlab.com/avisi/pec/squid:latest
