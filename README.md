# Squid

[![pipeline status](https://gitlab.com/avisi/pec/squid/badges/master/pipeline.svg)](https://gitlab.com/avisi/pec/squid/commits/master)

> Squid proxy in a Docker image

This repository contains a Dockerfile with a Squid proxy installation.

> The config / set-up in this project is currently still alpha and not ready for use.

## Table of Contents

- [Usage](#usage)
- [Contribute](#contribute)
- [Automated build](#automated build)
- [License](#license)

## Usage

Start the `registry.gitlab.com/avisi/pec/squid:3` image and publish port `3128/tcp`.

Example docker-compose file:

```yaml
version: '2'
services:
  squid:
    image: registry.gitlab.com/avisi/pec/squid:3
    ports: ['3128:3128']
```

Try it out with `wget`:

```bash
export http_proxy=http://localhost:3128
export https_proxy=http://localhost:3128
wget https://avisi.nl
```

### Config

Mount your own config file in the `/etc/squid/conf.d/` directory to overwrite the default ACL's. 

Multiple config files will be loaded from within this directory, as long as they match `*.conf`.

## Automated build

This image is build at least once a month automatically. All PR's are automatically build.

## Contribute

PRs accepted. All issues should be reported in the [Gitlab issue tracker](https://gitlab.com/avisi/pec/squid/issues).

## License

[MIT © Avisi B.V.](LICENSE)
