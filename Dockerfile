FROM registry.gitlab.com/avisi/base/centos:7

EXPOSE 3128/tcp

USER root
ENV SQUID_VERSION=3.5.20-12.el7_6.1 \
    SQUID_CACHE_DIR=/var/spool/squid \
    SQUID_LOG_DIR=/var/log/squid \
    SQUID_CONFIG_ACL=/etc/squid/conf.d \
    SQUID_USER=squid

RUN yum install -y squid-$SQUID_VERSION \
    && yum clean all \
    && rm -rf /var/cache/yum 

RUN usermod -a -G tty squid

RUN echo 'include /etc/squid/conf.d/*.conf' >> "/etc/squid/squid.conf" \
    && install -d -m 755 -o squid -g squid $SQUID_CONFIG_ACL

COPY squid.conf /etc/squid/squid.conf
COPY --chown=squid:squid conf/* $SQUID_CONFIG_ACL/

USER squid

COPY entrypoint.sh /usr/bin/entrypoint.sh
ENTRYPOINT ["/usr/bin/entrypoint.sh"]
